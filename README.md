# Org-Weave
Copyright (c) 2023 Radek Hnilica

Org-Weave is a program similar to Donald Knuth `weave` for org files.

When implementing org-tangle, I let the original .org file be a source
for PDF transformation.

However the resulted documents lack correct information about code
chunks.  So I tested some solution manually, and org-weave should do
that automatically.  So the original .org file will be processed by
org-weave into new .org whose parts are mangled for better output.

This project is hosted in ~/st/repo/sc/src/org-weave.
And the primary source will be published on GitLab
