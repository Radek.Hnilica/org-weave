# ! make
# $Id: Makefile,v 1.7 2022/12/23 13:05:38 radek Exp radekhnilica $
# $Source: /Users/radekhnilica/st/repo/sc/src/org-weave/src/Makefile,v $

# Get the system os we are running on
SYSTEM=$(shell uname -s)

# On MacBook I'm using different compiler and need to define it there.
ifeq ($(SYSTEM),Darwin)
CC	= gcc-12
endif

STD	:= -std=c11 -g
STACK	:=
WARNS	:= -Wall -Wextra -pedantic
CFLAGS	:= $(STD) $(STACK) $(WARNS)

# Where the org-tangle binary resides?  In this case we are using the
# development version.
#ORG_TANGLE=$(HOME)/st/src/org-tangle/src/org-tangle
#ORG_TANGLE=$(HOME)/st/repo/sc/src/org-tangle/src/org-tangle
ORG_TANGLE=org-tangle
ORG_WEAVE=./org-weave

SOURCES	= org-weave.c
OBJECTS	= $(SOURCES:.c=.o)
EXECUTABLE = org-weave
orgs	= $(wildcard *.org)	# Documents are recognised by '.org' extension
worgs	= $(orgs:.org=.worg)
pdfs	= $(orgs:.org=.pdf)
htmls	= $(orgs:.org=.html)
txts	= $(orgs:.org=.txt)
outputs = $(addprefix $(output)/pdf/,$(pdfs)) $(addprefix $(output)/txt/,$(txts))

# Directory for created pdf, html and texts.
output= $(HOME)/st/doc
VPATH = $(output)

# PanDoc options
PANDOC_OPTS	= --tab-stop=8
PANDOC_PDF_OPTS = $(PANDOC_OPTS) -V geometry:a4paper,margin=3cm --number-sections --toc
# I have different installation on MacBook so need to handle pandoc options.
ifeq ($(SYSTEM),Darwin)
PANDOC_PDF_OPTS += --pdf-engine xelatex
else
PANDOC_PDF_OPTS += --highlight-style tango
endif
# The tango style fails on host 'factory'.  Remove it when running on that host.

PANDOC_PDF_OPTS +=  $(COLOR_LINKS)
PANDOC_TXT_OPTS  = $(PANDOC_OPTS) -t plain
PANDOC_HTML_OPTS = $(PANDOC_OPTS) -t html -N
COLOR_LINKS = -V colorlinks=true -V linkcolor=blue -V urlcolor=red -V toccolor=gray


default: all

all:	$(SOURCES) $(EXECUTABLE) $(outputs) $(worgs)

clean:
	rm -v $(OBJECTS) $(EXECUTALBE) Makefile.deps *~ $(pdfs) $(txts)

install: $(EXECUTABLE)
	install -m 0500 $(EXECUTABLE) $(HOME)/.local/bin

org-weave.c : org-weave.org
	$(ORG_TANGLE) org-weave.org

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@


-include Makefile.deps
Makefile.deps:
	$(CC) $(CFLAGS) -MM *.[c] > Makefile.deps

### Wave the .org file to .worg file
%.worg: %.org
	$(ORG_WEAVE) -f -o $@ $<


### Pandoc Rules
# NOTICE that I supplied '--from=org' option as pandoc do not
# necesarily recognize '.worg' files.
#
$(output)/txt/%.txt: %.worg
	pandoc $(PANDOC_TXT_OPTS) --from=org -o $@ -f org $<
	cp $@ .

$(output)/pdf/%.pdf: %.worg
	pandoc $(PANDOC_PDF_OPTS) --from=org -o $@ -f org $<
	cp $@ .

# ORG to TXT (UTF-8) transformation
# %.txt: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-ascii-export-to-ascii

# # ORG to PDF usign LaTeX export
# %.pdf: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-latex-export-to-pdf
# 	rm -v $(basename $@).tex

# # ORG to HTML
# %.html: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-html-export-to-html

#Eof:$Id: Makefile,v 1.7 2022/12/23 13:05:38 radek Exp radekhnilica $
