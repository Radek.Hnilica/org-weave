#!/bin/bash
# $Id: cycle.sh,v 1.6 2023-09-19 12:49:20+02 radek Exp $
# $Source: /home/radek/st/repo/sc/src/scsim/src/cycle.sh,v $

typeset -r BUILD_STAMP=mark.ts
typeset -r TITLE=weave
typeset -r BIG_LOOP="10 minutes ago"
typeset -r DELAY=30s

while :; do
	# Check the time from last make
	if [[ $(date +%F_%T -r $BUILD_STAMP) < $(date +%F_%T -d "$BIG_LOOP") ]]; then
		title "$TITLE building"
		make	   # Build the outputs, binaries, reports, ...
		touch $BUILD_STAMP
	fi
	echo -n "INFO(cycle,$TITLE): $(date +%T) "
	title "$TITLE sleeping"
	sleep $DELAY &
	sleep_pid=$!
	echo sleep.pid=$sleep_pid
	echo $sleep_pid >sleep.pid
	wait $sleep_pid
done
